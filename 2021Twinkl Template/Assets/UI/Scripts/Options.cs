﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Options : MonoBehaviour
{
    [Header("Icons")]
    public Sprite muteOnIcon;
    public Sprite muteOffIcon;
    public Sprite expandOnIcon;
    public Sprite expandOffIcon;
    [Header("Buttons")]
    public Image muteBTN; 
    public Image expandBTN;

    void Start()
    {
        SetSoundState();
        SetScreenState();
    }

    // MUTE FUNCTIONS -------------------------------------------
    //Remember State of Mute Button When Changing Scenes
    public void ToggleSound()
    {
        //If the current state has not been set
        if (PlayerPrefs.GetInt("Muted", 0) == 0)
        {
            //Set state to 1 
            PlayerPrefs.SetInt("Muted", 1);
        } else {
            //Set state to 0
            PlayerPrefs.SetInt("Muted", 0);
        }
        SetSoundState();
    }

    //Set The Sound Toggle State
    private void SetSoundState()
    {
        if (PlayerPrefs.GetInt("Muted", 0) == 0)
        {
            AudioListener.volume = 0;
            muteBTN.sprite = muteOffIcon;
        } else {
            AudioListener.volume = 1;
            muteBTN.sprite = muteOnIcon;
        }
    }

    // EXPAND FUNCTIONS ----------------------------------------
    public void ToggleScreen()
    {
        //If the current state has not been set
        if (PlayerPrefs.GetInt("Full", 0) == 0)
        {
            //Set state to 1 
            PlayerPrefs.SetInt("Full", 1);
        } else {
            //Set state to 0
            PlayerPrefs.SetInt("Full", 0);
        }
        SetScreenState();
    }

    //Set The Sound Toggle State
    private void SetScreenState()
    {
        if (PlayerPrefs.GetInt("Full", 0) == 0)
        {
            Screen.fullScreenMode = FullScreenMode.Windowed;
            expandBTN.sprite = expandOnIcon;
        } else {
            Screen.fullScreenMode = FullScreenMode.ExclusiveFullScreen;
            expandBTN.sprite = expandOffIcon;
        }
    }
}
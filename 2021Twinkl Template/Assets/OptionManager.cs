﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class OptionManager : MonoBehaviour
{
    OptionsData optionsData;
    [Header("Icons")]
    public Sprite muteOnIcon;
    public Sprite muteOffIcon;
    public Sprite expandOnIcon;
    public Sprite expandOffIcon;
    [Header("Buttons")]
    public Image muteBTN; 
    public Image expandBTN;

    void Awake()
    {
        optionsData = GameObject.FindGameObjectWithTag("Options data").GetComponent<OptionsData>();
        SetSoundAndScreen();
    }

    void SetSoundAndScreen()
    {
        if(optionsData.muted)
        {
            AudioListener.volume = 0;
            muteBTN.sprite = muteOffIcon;
            Debug.Log("Sound off");
        } else {
            Debug.Log("Sound on");
        }

        if(optionsData.fullscreen)
        {
            Screen.fullScreenMode = FullScreenMode.ExclusiveFullScreen;
            expandBTN.sprite = expandOnIcon;
            Debug.Log("Screen is fullscreen");
        } else {
            Debug.Log("Screen is windowed");
        }
    }

    public void ToggleSound()
    {
        if(optionsData.muted)
        {
            AudioListener.volume = 1;
            muteBTN.sprite = muteOnIcon;
            Debug.Log("Sound on");
            optionsData.muted = false;
        } else {
            AudioListener.volume = 0;
            muteBTN.sprite = muteOffIcon;
            Debug.Log("Sound off");
            optionsData.muted = true;
        }
    }

    public void ToggleScreen()
    {
        if(optionsData.fullscreen)
        {
            Screen.fullScreenMode = FullScreenMode.Windowed;
            expandBTN.sprite = expandOnIcon;
            Debug.Log("Game is windowed");
            optionsData.fullscreen = false;
        } else {
            Screen.fullScreenMode = FullScreenMode.ExclusiveFullScreen;
            expandBTN.sprite = expandOffIcon;
            Debug.Log("Game is fullscreen");
            optionsData.fullscreen = true;
        }
    }
}